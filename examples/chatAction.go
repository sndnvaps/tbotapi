package main

import (
	"bitbucket.org/mrd0ll4r/tbotapi"
	"bitbucket.org/mrd0ll4r/tbotapi/examples/boilerplate"
	"fmt"
	"time"
)

func main() {
	apiToken := "123456789:Your_API_token_goes_here"

	updateFunc := func(update tbotapi.Update, api *tbotapi.TelegramBotAPI) {
		switch update.Type() {
		case tbotapi.MessageUpdate:
			msg := update.Message
			typ := msg.Type()
			if typ != tbotapi.TextMessage {
				//ignore non-text messages for now
				fmt.Println("Ignoring non-text message")
				return
			}
			// Note: Bots cannot receive from channels, at least no text messages. So we don't have to distinguish anything here

			// display the incoming message
			// msg.Chat implements fmt.Stringer, so it'll display nicely
			// we know it's a text message, so we can safely use the Message.Text pointer
			fmt.Printf("<-%d, From:\t%s, Text: %s \n", msg.ID, msg.Chat, *msg.Text)

			fmt.Printf("Sending ChatActionTyping to %s\n", msg.Chat)
			err := api.NewOutgoingChatAction(tbotapi.NewRecipientFromChat(msg.Chat), tbotapi.ChatActionTyping).Send()
			if err != nil {
				fmt.Printf("Error sending: %s\n", err)
				return
			}
			time.Sleep(2 * time.Second)

			// clear chat action
			outMsg, err := api.NewOutgoingMessage(tbotapi.NewRecipientFromChat(msg.Chat), "Finished typing.").Send()

			if err != nil {
				fmt.Printf("Error sending: %s\n", err)
				return
			}
			fmt.Printf("->%d, To:\t%s, Text: %s\n", outMsg.Message.ID, outMsg.Message.Chat, *outMsg.Message.Text)

			fmt.Printf("Sending ChatActionFindLocation to %s\n", msg.Chat)
			err = api.NewOutgoingChatAction(tbotapi.NewRecipientFromChat(msg.Chat), tbotapi.ChatActionFindLocation).Send()
			if err != nil {
				fmt.Printf("Error sending: %s\n", err)
				return
			}
			time.Sleep(2 * time.Second)

			// clear chat action and tell them we're done
			outMsg, err = api.NewOutgoingMessage(tbotapi.NewRecipientFromChat(msg.Chat), "Done").Send()

			if err != nil {
				fmt.Printf("Error sending: %s\n", err)
				return
			}
			fmt.Printf("->%d, To:\t%s, Text: %s\n", outMsg.Message.ID, outMsg.Message.Chat, *outMsg.Message.Text)
		case tbotapi.InlineQueryUpdate:
			fmt.Println("Ignoring received inline query: ", update.InlineQuery.Query)
		case tbotapi.ChosenInlineResultUpdate:
			fmt.Println("Ignoring chosen inline query result (ID): ", update.ChosenInlineResult.ID)
		default:
			fmt.Printf("Ignoring unknown Update type.")
		}
	}

	// run the bot, this will block
	boilerplate.RunBot(apiToken, updateFunc, "ChatAction", "Demonstrates chat actions")
}
